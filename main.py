import os 
import xlrd
import xlwt
import re
from docxtpl import DocxTemplate

class main():

    listVulUniq = []                                             # :list de las vulnerabilidades sin repetir 
    listVulUniqLevel = [
        "Critical",
        "High",
        "Low",
        "Medium"
    ]   
    docUniqVul  = ""    

    def getVulUniq(self, *args, **kwargs):

        docName = kwargs["docname"]
        sheetName = kwargs["sheetname"]

        # Leyendo Excel
        rd = xlrd.open_workbook(docName)
        sheet =  rd.sheet_by_name(sheetName)
     
        
        for rownum in range(sheet.nrows):
            descripVul = sheet.row_values(rownum)[7]
            riskVul = sheet.row_values(rownum)[3]

            if descripVul not in self.listVulUniq and riskVul in self.listVulUniqLevel:
                self.listVulUniq.append(descripVul)

        # Preparando nuevo archivo con vuls uniq
        newFile = xlwt.Workbook()
        sheet1 = newFile.add_sheet("Uniq")
        counterRow = 0

        for vul in self.listVulUniq:
            sheet1.write(counterRow, 0, vul)
            counterRow += 1
    
        self.docUniqVul = "test2.xls"
        newFile.save(self.docUniqVul)
        return self.listVulUniq


    def makeTableInWord(self, *args, **kwargs):
        infoVuls = []                                                            # Lista con las vulnerabilidades sumarisadas y filtradas.
        finalResults = []                                                         # Lista con los resultados finales. 
        nessusResults = []

        # leemos archivo con las vulnerabilidades unicas
        docVulUniq = xlrd.open_workbook("test.xls")                              # Documentos con las vulnerabilidades unicas.
        sheetVulUniq =  docVulUniq.sheet_by_name("Uniq")                         # Hoja del documento con las vulnerabilidades unicas.

        # Leemos archivo del nexxus
        docReportNessus = xlrd.open_workbook("depurado.xlsx")                    # Documento exportado del nessus.
        sheetReportNessus =  docReportNessus.sheet_by_name("depurado")           # Hoja del documento exportado por el nessus.

        

        for rowUniqDoc in range(sheetVulUniq.nrows):

            nameVulUniq = sheetVulUniq.row_values(                          # Obtenemos el nombre de la vulnerabilidad en el documento de vulnerabilidades unicas
                rowUniqDoc
            )[0]

            desVulUniq = sheetVulUniq.row_values(                           # Obtenemos la descripcion de la vulnerabilidad en el documento de vulnerabilidades unicas
                   rowUniqDoc
            )[1]

            recommVulUniq = sheetVulUniq.row_values(                        # Obtenemos la recomendacion de la vulnerabilidad en el documento de vulnerabilidades unicas
                   rowUniqDoc
            )[2]

                
            

            '''Validamos si esta la vulnerabilidad 
            en las filtradas
            '''
                                   
            if len(nameVulUniq) > 0:    
                finalResults.append({
                    "name": nameVulUniq,
                    "desp": desVulUniq,
                    "recomm": recommVulUniq,
                    "ip": [],
                    "risk": "",
                })


        
        for rowNessusDoc in range(sheetReportNessus.nrows):
            NameVulNessus = sheetReportNessus.row_values(                        # Obtenemos el nombre de la vulnerabilidad index 7.
                rowNessusDoc
            )[7]

            riskVulNessus = sheetReportNessus.row_values(                        # Obtenemos el nivel de riesgo de la vulnerabilidad index 7.
                rowNessusDoc
            )[3]

            ipVulNessus = sheetReportNessus.row_values(                        # Obtenemos el ip de la vulnerabilidad index 7.
                rowNessusDoc
            )[4]

            portVulNessus = sheetReportNessus.row_values(                        # Obtenemos el port de la vulnerabilidad index 7.
                rowNessusDoc
            )[6]


            nessusResults.append({
                "name": NameVulNessus,
                "ip": ipVulNessus,
                "risk": riskVulNessus,
                "port": portVulNessus,
            })
            
        for nessusResult in nessusResults:
            for index, finalResult in enumerate(finalResults):

                if finalResult["name"] == nessusResult["name"]:
                    host = '{0}:{1}'.format(
                            nessusResult["ip"], 
                            int(nessusResult["port"])
                        )
                    
                    finalResults[index]["risk"] = nessusResult["risk"]
                    finalResults[index]["ip"].append(
                        host
                    )
                
               
                    
        for finalResult in finalResults:
            finalResult["name"] =  re.sub('[^a-zA-Z0-9 \n\.]', '', finalResult["name"])
            finalResult["ip"] = set(finalResult["ip"])
            
        '''Creando el archivo ejecutivo '''
        countRow = 0
        newDocEjec = xlwt.Workbook()
        sheetDocEjec = newDocEjec.add_sheet("Ejecutivo")
        for vul in finalResults:
            sheetDocEjec.write(countRow, 0, vul["name"])
            sheetDocEjec.write(countRow, 1, vul["risk"])
            countRow += 1
        newDocEjec.save("ejecutivo.xls")   


        '''Creando archivo en word renderizando.
        '''
        context = { "cntxt": finalResults}

        doc = DocxTemplate("template.docx")
        doc.render(context)
        doc.save("test.docx")
        return finalResults